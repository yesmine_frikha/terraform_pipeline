resource "aws_instance" "TM_Yesmine" {
  ami                           = var.ami_id
  instance_type                 = var.instance_type
  associate_public_ip_address   = "true"
  security_groups               = [aws_security_group.sec_group.name]
  vpc_security_group_ids        = [aws_security_group.sec_group.id]
  key_name                      = aws_key_pair.ssh_public.key_name
  user_data = <<-EOF
                #!bin/bash
                echo "PubkeyAcceptedKeyTypes=+ssh-rsa" >> /etc/ssh/sshd_config.d/10-insecure-rsa-keysig.conf
                systemctl reload sshd
                echo "${tls_private_key.ssh.private_key_pem}" >> /home/ubuntu/.ssh/id_rsa
                chown ubuntu /home/ubuntu/.ssh/id_rsa
                chgrp ubuntu /home/ubuntu/.ssh/id_rsa
                chmod 400   /home/ubuntu/.ssh/id_rsa
                EOF

  tags = {
    Name = "TM_Yesmine"
  }
}
