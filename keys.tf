resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "local_file" "ssh_private" {
    filename = "ssh_private.pem"
    file_permission = "400"
    content  = tls_private_key.ssh.private_key_pem
}

resource "aws_key_pair" "ssh_public" {
  key_name   = "ssh_public"
  public_key = tls_private_key.ssh.public_key_openssh
}